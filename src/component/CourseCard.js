import {useState} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Coursecard({courseProp}){
	// console.log(courseProp);
	// console.log(typeof courseProp);

	//object destructuring

	// const {name, description, price, activeObject =true} = this.props


	const {name, description, price, _id} = courseProp


	// react hooks - useStae -> store its state
	//Syntax:
	 	//const [getter, setter] = useState(initialGetterValue);
/*	const [count, setCount] = useState(0);
	console.log(useState(0));

	function enroll(){
		setCount(count + 1);
		console.log(`Enrollees: ${count}`);
	};*/
/*
	const [count, setCount] = useState(10);
	const [ecount, esetCount] = useState(0);
	

	function enroll(){

			if(count === 0 && ecount === 10){
				alert("No more Seats Available");
			}
			else {
			setCount(count - 1)
			esetCount(ecount +1);
							
			}
		
	};
*/

	return (
		<Row className="mt-3 mb-3">
			
			<Col>
				<Card className="p-3 mb-3">
					<Card.Body>
						<Card.Title className="fw-bold">
							{name}
						</Card.Title>
						<Card.Subtitle>
							Course Description:
						</Card.Subtitle>
						<Card.Text>
							{description}
						</Card.Text>
						<Card.Subtitle>
							Price:
						</Card.Subtitle>
						<Card.Text>
							{price}
						</Card.Text>
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
													
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
};