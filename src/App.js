//import component AppNapbar
import {useState, useEffect} from 'react';
import AppNavbar from './component/AppNavbar';
import CourseView from './component/CourseView';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
// import Banner from './component/Banner';
// import Highlight from './component/Highlight';
import {Container} from 'react-bootstrap';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import './App.css';
import {UserProvider} from './UserContext';

function App() {
const [user, setUser] = useState({	
	id: null,
	isAdmin: null
});

const unsetUser = () => {
	localStorage.clear();
};

useEffect(() => {
	fetch('http://localhost:4000/users/getUserDetails', {
		headers: {
			Authorization: `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if(typeof data._id !== "undefined"){
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		} else {
			setUser({
				id: null,
				isAdmin: null
			});
		};
	});
	

}, []);

	return (
		<>
			<UserProvider value={{user, setUser, unsetUser}}>
				<Router>
					<AppNavbar/>
					<Container>
						<Routes>
							<Route exact path ="/" element={<Home/>}/>
							<Route exact path ="/courses" element={<Courses/>}/>
							<Route exact path ="/courseView/:courseId" element={<CourseView/>}/>
							<Route exact path ="/register" element={<Register/>}/>
							<Route exact path ="/login" element={<Login/>}/>
							<Route exact path ="/logout" element={<Logout/>}/>
							<Route exact path ="*" element={<Error/>}/>
						</Routes>
					</Container>
				</Router>
			</UserProvider>
		</>
		)
}

export default App;
