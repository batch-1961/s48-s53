import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
		
	const [firstName,setFirstName] = useState('');
	const [lastName,setLastName] = useState('');
	const [mobileNo,setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [pword2, setPword2]= useState('');

	const [isActive, setIsActive] = useState(false);
	
	// console.log(email);
	// console.log(pword1);
	// console.log(pword2);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email that you're trying to register alreay exist"
				})
			} else {

				fetch('http://localhost:4000/users', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email:email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Thank you for registering'
							})

						history("/login")
						
					} else {
						Swal.fire({	
							title: 'Registration failed',
							icon: 'error',
							text: 'Something went wrong, try again'
						})
					}		
				})
			};
		});

		setEmail('');
		setPassword('');
		setFirstName('');
		setLastName('');
		setMobileNo('');
		// // setPword2('');

		
	}

	useEffect(() => {
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, password, firstName, lastName, mobileNo]);
	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>

		<h1>Register Here:</h1>
		<Form onSubmit = {e => registerUser(e)}>


			<Form.Group controlId="userfirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="string"
					placeholder="Enter your first name here"
					required
					value = {firstName}
					onChange = {e => setFirstName(e.target.value)}
				/>				
			</Form.Group>

			<Form.Group controlId="userlastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="string"
					placeholder="Enter your last name here"
					required
					value = {lastName}
					onChange = {e => setLastName(e.target.value)}
				/>				
			</Form.Group>

			<Form.Group controlId="usermobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="string"
					placeholder="Please input your 11-digit mobile number here"
					required
					value = {mobileNo}
					onChange = {e => setMobileNo(e.target.value)}
				/>				
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value = {email}
					onChange = {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone  else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value = {password}
					onChange = {e => setPassword(e.target.value)}
				/>
			</Form.Group>

			
		{ isActive ? 
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Register</Button>
		}

		</Form>
		</>
		)
}