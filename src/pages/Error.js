import {Link} from 'react-router-dom';
import { Col, Button} from 'react-bootstrap';


export default function PageNotFound(){

  return(
    <div>
      <Col className ="mt-5 mb-3">
        <h1>404 - Not Found</h1>
        <h1>Page not found.</h1>
        <Button as={Link} to="/" variant="info">Back Home</Button>
      </Col>
    </div>
  )
};
