
import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [pword1, setPword1] = useState('');
	
	const [isActive, setIsActive] = useState(false);
	
	function loginUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pword1
			})
		})
		.then(res => res.json())

		.then(data => {
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				})

			} else
				{
					Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})
				}

		});

		// localStorage.setItem("email", email)

		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		setEmail('');
		setPword1('');
			
		
	};

		const retrieveUserDetails = (token) => {

			fetch('http://localhost:4000/users/getUserDetails', {
					headers: {
						Authorization: `Bearer ${token}`
					}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			})
		};

	useEffect(() => {
		if(email !== '' && pword1 !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}

	}, [email, pword1]);


	return(
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		
		<>
		<h1>Login Here:</h1>
		<Form onSubmit = {e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value = {email}
					onChange = {e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					Log-in with your email
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value = {pword1}
					onChange = {e => setPword1(e.target.value)}
				/>
			</Form.Group>

			<p>Not yet registered? <Link to="/register">Register Here</Link></p>
			
			{ isActive ? 
				<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Login</Button>
				:
				<Button className="mt-3 mb-5" variant="info" type="submit" id="submitBtn" disabled>Login</Button>
			}

		</Form>
		</>
		)
}