import coursesData from '../data/coursesData';
import CourseCard from '../component/CourseCard';

import {useState, useEffect} from 'react';

export default function Courses(){
	// console.log(coursesData);
	// console.log(coursesData[0]);

	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch("http://localhost:4000/courses")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp = {course}/>
			)
			}));
		});

	}, []);


	//map() name of paramater is course only can be anything. key=course id, index of data in coursesData, courseProp is from destrcuturing of course in coursecard.js

	// const courses = coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 		)
	// })


	return (
		<>
		<h1>Available Courses:</h1>		
		{courses}
		</>

		)
}